function createResult(error, data) {
  const result = {};
  if (error) {
    // if there is any error
    result["status"] = "error";
    // result.status = 'error'

    result["error"] = error;
    // result.error = error
  } else {
    // there is no error (success)
    result["status"] = "success";
    result["data"] = data;
  }

  return result;
}
const mysql = require("mysql2");

const openConnection = () => {
  const connection = mysql.createConnection({
    host: "db",
    user: "root",
    password: "root",
    database: "mydb",
  });

  connection.connect();

  return connection;
};
module.exports = {
  createResult,
  openConnection,
};
